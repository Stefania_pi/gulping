// assuming DOM is loaded

document.getElementById("run").addEventListener("click", () => {
    let user = document.getElementById("user").value;
    if(user.length == 0) {
        user = 'Unkown';
    }

    document.getElementById("target").innerText = 'Hello, ' + user + '!';
});